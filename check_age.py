import pprint
import json
from datetime import datetime
from django.core.mail import send_mail
from extras.reports import Report
from django.template import Template, Context

# The object to be imported.
from dcim.models import Device

# Imported object related to the report.
OBJECT = Device

# Object attribute to be checked
ATTRIBUTE = 'description'

# Parameteres for filtering objects specified in the OBJECT variable.
# In the case of an empty dictionary, all objects will be returned.
# -> (equal to .all() method)
FILTER = {
    #'device_role__slug': 'l3-switch',
    #'name__startswith': 'sw',
    #'serial': '123456789'
}

# Provide list of years to check.
PURCHASE_AGE = [
    3,
    5,
    7
]

# Need to list all objects with empty ATTRIBUTE
LOG_EMPTY_ATTRIBUTE = False

# Datetime format of the specified attribute for the comparison.
DATETIME_FORMAT = '%Y-%m-%d'

# Need for sending the Email about the generated report
SEND_EMAIL = False

# Parametres for Email, if the variable SEND_EMAIL is True
EMAIL = {
    'subject': 'NetBox: Check Age Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ]
}

# Template for Email written in Django version of Jinja
EMAIL_FORMAT = """
{% for keys, values in data.items %}
{{ keys|title }}:
            INFO({{ values.info|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'info' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            SUCCESS({{ values.success|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'success' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            FAILURE({{ values.failure|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'failure' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            WARNING({{ values.warning|safe }})
                {% spaceless %}
            {% for name in values.log %}
                {% spaceless %}
                    {% if name.1 == 'warning' %}
                        {{ name.2|safe }}  -  {{ name.3|safe }}
                    {% endif %}
                {% endspaceless %}
            {% endfor %}
                {% endspaceless %}
{% endfor %}
"""

# Timeout for the report run
JOB_TIMEOUT = 300

class PurchaseReport(Report):

    description = 'Check Age of the device'
    job_timeout = JOB_TIMEOUT

    @property
    def name(self):
        return 'Age of the device'

    def __init__(self):
        self._create_test_methods()
        super().__init__()

    def _create_test_methods(self) -> None:
        datetime_today = datetime.today()
        result = {}
        for obj in OBJECT.objects.filter(**FILTER):
            try:
                obj_attr = getattr(obj, ATTRIBUTE)

                obj_datetime = datetime.strptime(
                    str(obj_attr),
                    DATETIME_FORMAT
                ).date()
                date_sub = datetime_today.year - obj_datetime.year

                for year in sorted(PURCHASE_AGE, reverse=True):
                    key = (year, obj.device_type.model, )
                    if date_sub > year:
                        result[key] = result.get(key, []) + [obj]
                        
                        break
            except Exception:
                continue

        for (year, device_type), objs in result.items():
            setattr(
                self.__class__,
                f'test_{year}_{device_type}',
                staticmethod(self._purchase_wrapper(year, objs))
            )

    def post_run(self):
        data = json.dumps(self._results)
        if SEND_EMAIL:
            email_template = Template(EMAIL_FORMAT)
            context = Context(
                {
                    'data': json.loads(data)
                }
            )
            message = email_template.render(context)
            send_mail(
                message=message,
                **EMAIL
            )

    def _purchase_wrapper(self, year, objs):

        def run_test():
            self._check_purchase(year, objs)

        return run_test

    def _check_purchase(self, year, objs):
        for obj in objs:
            self.log_info(obj, f'Age of purchase is over {year} year(s).')

    def check_object_attribute_existence(self):
        object_attributes = dir(OBJECT)
        return ATTRIBUTE in object_attributes

    def test_object_attribute_integrity(self):
        if not self.check_object_attribute_existence():
            self.log(f'Object has no attribute {ATTRIBUTE}.')
            return

        all_valid = True
        for obj in OBJECT.objects.filter(**FILTER):
            attr_value = getattr(obj, ATTRIBUTE)
            if not attr_value:
                if LOG_EMPTY_ATTRIBUTE:
                    self.log_warning(obj, 'Attribute is empty')
                continue
            try:
                datetime.strptime(str(attr_value), DATETIME_FORMAT).date()
                self.log_success(obj, f'Value {attr_value} is valid.')
            except ValueError:
                self.log_failure(obj, f'Value {attr_value} is not valid.')
                all_valid = False
        if all_valid:
            self.log('Everything is okay.')
