# Check Objects Age

Report je určen pro kontrolu stáří konkrétních objektů v Netboxu.

## Instalace
- **[Verze 3.4.X a níže]** Report je nutno vložit do složky reports, která má nejčastěji tuto cestu netbox/reports/.
- **[Verze 3.5.X a výše]** Report je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Reports -> Add
- Viz https://docs.netbox.dev/en/stable/customization/reports/

## Výstup
![alt text](/images/Age_of_the_device.png)

## Prerekvizity
1. Vytvořen objekt pro kontrolu.
2. Správně vyplněný datum pro srovnání v zvoleném atributu objektu.
3. Vyplněné globální proměnné v reportu

## Postup fungování Reportu
1. Report nejprve projde všechny objekty po aplikování filtru a zkontroluje, zdali mají tyto objekty správně nastavený specifikovaný atribut. V případě nesprávného formátu nebo prázdné hodnoty bude uživatel informován.
2. Poté budou opět procházena všechna zařízení a bude se kontrolovat, zdali rok ve specifikovaném atributu spadá do nějakého ze zadaných roků.
3. Report následně seřadí nalezená zařízení ve vzestupném pořadí podle definovaných let a podle Device Types.
4. Report informuje uživatele o výsledku.
5. V případě nastavení, se na konci odešle email adresátům o výsledku reportu.

## Proměnné
**Pro správné fungování reportu je nutné upravit globální proměnné uvnitř kódu!!!**

#### OBJECT
 - Táto proměnna představuje objekt, který se má kontrolovat. 
 - Je nutné, aby byl tento objekt správně importován.
 - Výchozí hodnota je nastavena na Device.
```python
from dcim.models import Device

OBJECT = Device
```
#### ATTRIBUTE 
 - Táto proměnna představuje atribut, ve kterém je umístěn datum pro srovnání konkrétního objektu.
 - Výchozí hodnota je nastavena na Description.
```python
ATTRIBUTE = 'description'
```

#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh reportu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```
#### DATETIME_FORMAT
 - Táto proměnná představuje formát datumu, který má být porovnán.
 - Výchozí hodnota je nastavena na "Y-m-d".
```python
DATETIME_FORMAT = '%Y-%m-%d'
```

#### FILTER
 - Táto proměnná představuje atributy, podle kterých se má výběr zařízení filtrovat. 
 - V případě ponechání prázdných složených závorek nebude použit žádný filtr.
 - Výchozí hodnota je nastavena na Null.
```python
FILTER = {
    "device_role__slug" : "l3-switch",
    "name__startswith" : "switch-",
    "serial" : "123456789"
}
```
#### PURCHASE_AGE
 - Táto proměnná představuje roky, podle kterých bude report seskupovat a porovnávat věk zařízení. 
 - Hodnoty jsou v reportu seřazeny vzestupně - nezalezí na pořadí hodnot v proměnné
 - Výchozí hodnota je nastavena na 3, 5 a 7 let.
```python
PURCHASE_AGE = [
    3,
    5,
    7
]
```
#### LOG_EMPTY_ATTRIBUTE
 - Táto proměnna představuje možnost logování objektů, které nemají vyplněný ATTRIBUTE. 
 - Ve výchozím stavu je nastavena na False.
```python
LOG_EMPTY_ATTRIBUTE = False
```
#### SEND_EMAIL
 - Táto proměnná představuje možnost posílání emailu o stavu reportu po jeho dokončení. 
 - Výchozí hodnota je nastavena na False.
 ```python
SEND_EMAIL = False
 ```
#### EMAIL_FORMAT
 - Tato proměnná představuje formátování pro zaslaný email.
 - Pro zpracování je použita šablona se syntaxi Jinja v Django provedení.
 - Výsledek reportů je do šablony vložen přes proměnnou s názvem data.
 ```python
EMAIL_FORMAT = """

"""
 ```
#### EMAIL
 - Táto proměnná představuje dictionary s parametry pro správné odesílání emailů. 
 - V kódu reportu se skládá z následujících parametrů:
    - **subject** - Předmět emailu
    - **from_email** - Odesílatel emailu
    - **recipient_list** - list příjemců mailu
    - Další parametry se nacházejí v konfiguraci NetBoxu v souboru configuration.py 
        - Dokumentace: **https://docs.djangoproject.com/en/4.2/topics/email/**
```python
EMAIL = {
    'subject': 'NetBox: Check Age Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ],
}
```

_**Vzorový email**_
```python
Test_Purchase_Over_1_Year:
            INFO(6)
                    device1  -  /dcim/devices/3/
               
                    device2  -  /dcim/devices/7/
               
                    device3  -  /dcim/devices/2/
               
                    device4  -  /dcim/devices/1/
               
                    device5  -  /dcim/devices/9/
               
                    device6  -  /dcim/devices/4/
            SUCCESS(0)
                   
            FAILURE(0)
                   
            WARNING(0)
               

Test_Purchase_Over_7_Year:
            INFO(1)
                    device7  -  /dcim/devices/10/
            SUCCESS(0)
                   
            FAILURE(0)
                   
            WARNING(0)
```
**Pro správné fungování je nutně vyplnění atributů v .env souboru "netbox.env"**
```python
EMAIL_FROM=netbox@domain.com
EMAIL_PASSWORD=
EMAIL_PORT=25
EMAIL_SERVER=relay.domain.com
EMAIL_SSL_CERTFILE=
EMAIL_SSL_KEYFILE=
EMAIL_TIMEOUT=5
EMAIL_USERNAME=
```

